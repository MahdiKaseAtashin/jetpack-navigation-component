package com.mahdikaseatashin.jetpacknavigationcomponent

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_choose_recipient.*
import kotlinx.android.synthetic.main.fragment_specify_amount.*

class ChooseRecipientFragment : Fragment(), View.OnClickListener {
    lateinit var navController: NavController
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        view.findViewById<Button>(R.id.next).setOnClickListener(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_choose_recipient, container, false)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.next -> {
                if (!TextUtils.isEmpty(edt_next.text.toString())) {
                    Log.e(Companion.TAG, "onClick")
                    val bundle = bundleOf("Key" to edt_next.text.toString())
                    navController!!.navigate(
                        R.id.action_chooseRecipientFragment2_to_specifyAmountFragment2,
                        bundle
                    )
                }
            }
        }
    }

    companion object {
        private const val TAG = "ChooseRecipientFragment"
    }
}
